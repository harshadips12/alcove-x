const hbs = require("handlebars");
const fs = require("fs");
const path = require("path");

// renders the html template with the given data and returns the html string
hbs.registerHelper({
    eq: (v1, v2) => v1 === v2,
    ne: (v1, v2) => v1 !==
        v2,
    lt: (v1, v2) => v1 < v2,
    gt: (v1, v2) => v1 > v2,
    lte: (v1, v2) =>
        v1 <= v2,
    gte: (v1, v2) => v1 >= v2,
    and() {
        return
        Array.prototype.every.call(arguments, Boolean);
    },
    or() {
        return
        Array.prototype.slice.call(arguments, 0, -1).some(Boolean);
    }
});

console.log('registered');

function renderTemplate(data, templateName) {
    const html = fs.readFileSync(path.join(__dirname, `templates/${templateName}.hbs`), {
        encoding: "utf-8",
    });
    // console.log(data);
    // creates the Handlebars template object
    const template = hbs.compile(html);

    // renders the html template with the given data
    const rendered = template(data);
    return rendered;
}

module.exports = renderTemplate;